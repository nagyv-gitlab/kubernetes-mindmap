<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1586510718222" ID="ID_1492779144" MODIFIED="1587045612333" TEXT="Kubernetes @ GitLab">
<node CREATED="1586510761477" ID="ID_576851114" MODIFIED="1586511549802" POSITION="right" TEXT="Deploying to Kubernetes">
<icon BUILTIN="yes"/>
<node CREATED="1586510772905" ID="ID_1441285070" MODIFIED="1586510780790" TEXT="Off the shelf apps">
<node CREATED="1586510786848" ID="ID_82510001" MODIFIED="1586510788853" TEXT="Helm"/>
<node CREATED="1587492951820" ID="ID_1293316381" MODIFIED="1587492958602" TEXT="Troubleshooting"/>
</node>
<node CREATED="1586510781204" ID="ID_1261363578" MODIFIED="1586510786376" TEXT="Bespoke apps">
<node CREATED="1586510789284" ID="ID_1030203547" MODIFIED="1586510799680" TEXT="kustomize">
<node CREATED="1586510799681" ID="ID_72473639" MODIFIED="1586510799681" TEXT=""/>
</node>
<node CREATED="1587045916804" ID="ID_46775580" MODIFIED="1587045920870" TEXT="canary deployments"/>
<node CREATED="1587045921567" ID="ID_165396288" MODIFIED="1587045925576" TEXT="blue-green dpleoyments"/>
<node CREATED="1587045926629" ID="ID_793823943" MODIFIED="1587045962811" TEXT="rolling updates"/>
<node CREATED="1587045968100" ID="ID_1800816408" MODIFIED="1587045979501" TEXT="rollbacks"/>
<node CREATED="1587492960563" ID="ID_1123189942" MODIFIED="1587492963760" TEXT="Troubleshooting"/>
</node>
<node CREATED="1586511857583" ID="ID_231912828" MODIFIED="1587138698521" TEXT="Managed services to integrate with">
<node CREATED="1586512198052" ID="ID_1582455134" MODIFIED="1586512199557" TEXT="DB"/>
<node CREATED="1586512199808" ID="ID_691830180" MODIFIED="1586512201282" TEXT="Storage"/>
<node CREATED="1586512201897" ID="ID_1369219489" MODIFIED="1586512204965" TEXT="Load balancers"/>
</node>
<node CREATED="1587044595907" ID="ID_1557412609" MODIFIED="1587044603591" TEXT="Namespaces management">
<node CREATED="1587045125863" ID="ID_1942885720" MODIFIED="1587045134803" TEXT="restrict team / project to a namespace"/>
<node CREATED="1587045135390" ID="ID_1325971388" MODIFIED="1587045143825" TEXT="quotas per namespace"/>
</node>
</node>
<node CREATED="1586510806299" ID="ID_319633602" MODIFIED="1586511993327" POSITION="left" TEXT="Managing Kubernetes">
<node CREATED="1586510814887" ID="ID_1624016183" MODIFIED="1586511546491" TEXT="Who manages the cluster?">
<icon BUILTIN="yes"/>
<node CREATED="1586510821947" ID="ID_1856764659" MODIFIED="1586510829671" TEXT="&lt; 30 engineers">
<node CREATED="1586510829672" ID="ID_1604642168" MODIFIED="1586510834333" TEXT="Senior engineer"/>
<node CREATED="1587045800820" ID="ID_1071919090" MODIFIED="1587045807330" TEXT="kubernetes might be overkill"/>
</node>
<node CREATED="1586510836831" ID="ID_1025001489" MODIFIED="1586510841479" TEXT="30 - 100 engineers">
<node CREATED="1586510841480" ID="ID_1587129042" MODIFIED="1586510848618" TEXT="Few DevOps engineers"/>
</node>
<node CREATED="1586510849037" ID="ID_437543882" MODIFIED="1586510852458" TEXT="100+ engineers">
<node CREATED="1586510852460" ID="ID_638254983" MODIFIED="1586510860777" TEXT="DevOps / Platform team"/>
</node>
</node>
<node CREATED="1586510872281" ID="ID_406525710" MODIFIED="1586510882379" TEXT="Adding cluster to GitLab">
<node CREATED="1586510882380" ID="ID_868351889" MODIFIED="1587492937666" TEXT="Existing cluster">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      When the user imports an already existing cluster, the cluster might be&#160;&#160;at a provider otherwise supported by GitLab. Currently, we treat all these clusters as being user provided, and don't even try to allow the user to open the provider's GUI from our interface.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1586510889791" ID="ID_716796160" MODIFIED="1586510895829" TEXT="New cluster">
<node CREATED="1586510903879" ID="ID_1410652432" MODIFIED="1586510906074" TEXT="GKE"/>
<node CREATED="1586510908810" ID="ID_85508587" MODIFIED="1586510910429" TEXT="EKS"/>
<node CREATED="1586510910957" ID="ID_341971977" MODIFIED="1586510945755" TEXT="DigitalOcean"/>
<node CREATED="1586510946222" ID="ID_1255578411" MODIFIED="1586510948436" TEXT="Azure"/>
</node>
<node CREATED="1586511968276" ID="ID_419060320" MODIFIED="1586511983665" TEXT="Managed vs self-managed cluster"/>
<node CREATED="1587044108474" ID="ID_281691382" MODIFIED="1587129749689" TEXT="minikube and k3s clusters for evaluation,  local development">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      There are a few small, one-node cluster solutions that are often used for local development, testing and evaluation. They might be used to evaluate our offering, or once GitLab is their choice it might be used by their developers to test their own applications.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1586511057439" ID="ID_1173102155" MODIFIED="1586512327912" TEXT="Specific features">
<node CREATED="1586511075382" ID="ID_890223937" MODIFIED="1587129763181" TEXT="Horizontal Pod Autoscaling"/>
<node CREATED="1586511079790" ID="ID_822904290" MODIFIED="1587129960608" TEXT="Secrets management">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Kubernetes provides its own Secret resource. Still, it's not adequate for several use-cases where other tools, like HashiCorp's Vault might be preferred inside of K8s.
    </p>
    <p>
      
    </p>
    <p>
      e.g: https://medium.com/@harsh.manvar111/kubernetes-secret-vs-vault-fb57d75ce553
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1586511139945" ID="ID_989685014" MODIFIED="1587138605605" TEXT="Access rights">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      The basic approach to access rights management is to always allow the least privilege and try not to be a bottleneck.
    </p>
  </body>
</html></richcontent>
<node CREATED="1586511450917" ID="ID_1843428055" MODIFIED="1586511456514" TEXT="cluster admin rights"/>
<node CREATED="1586511457357" ID="ID_119834444" MODIFIED="1586511479823" TEXT="namespace-level access"/>
<node CREATED="1586511901979" ID="ID_1251660540" MODIFIED="1586511911130" TEXT="authentication backend"/>
<node CREATED="1587044178166" ID="ID_1823283418" MODIFIED="1587044244775" TEXT="locked down clusters"/>
<node CREATED="1587044247008" ID="ID_1187734917" MODIFIED="1587044252043" TEXT="air gapped networks"/>
</node>
<node CREATED="1586511494975" ID="ID_1290872995" MODIFIED="1586511776562" TEXT="Resource limits beyond yaml">
<node CREATED="1586511761693" ID="ID_676653807" MODIFIED="1586511795306" TEXT="Scheduled cluster starts / ends"/>
<node CREATED="1586512224390" ID="ID_543645328" MODIFIED="1586512227627" TEXT="Cost management"/>
</node>
<node CREATED="1586512241890" ID="ID_185287495" MODIFIED="1586512245915" TEXT="Chaos engineering"/>
<node CREATED="1586512330068" ID="ID_793588706" MODIFIED="1586512335557" TEXT="Underlying OS"/>
<node CREATED="1586512406212" ID="ID_188513420" MODIFIED="1586512412557" TEXT="Resource tagging"/>
<node CREATED="1587044834812" ID="ID_27000936" MODIFIED="1587045063528" TEXT="Various VM types (preemptible, shielded)"/>
</node>
<node CREATED="1586511993328" ID="ID_1169293872" MODIFIED="1586512008466" TEXT="Upgrade K8s">
<node CREATED="1586512010318" ID="ID_531909465" MODIFIED="1586512014457" TEXT="Upgrade master node"/>
<node CREATED="1586512016838" ID="ID_963780084" MODIFIED="1586512019867" TEXT="Upgrade worker nodes"/>
</node>
<node CREATED="1587044704112" ID="ID_1480218846" MODIFIED="1587116202829" TEXT="Managing nodes">
<icon BUILTIN="idea"/>
<node CREATED="1587044708010" ID="ID_645087292" MODIFIED="1587044716362" TEXT="Upgrade nodes (for resizing)"/>
<node CREATED="1587044728003" ID="ID_286225656" MODIFIED="1587044731726" TEXT="Disk configurations"/>
<node CREATED="1587044732304" ID="ID_535556383" MODIFIED="1587044738848" TEXT="SSH into a node"/>
</node>
<node CREATED="1587044762449" ID="ID_302171546" MODIFIED="1587044765078" TEXT="Networking">
<node CREATED="1587044766037" ID="ID_356815706" MODIFIED="1587044774735" TEXT="private cluster in a VPC"/>
<node CREATED="1587044775959" ID="ID_1583946959" MODIFIED="1587044784533" TEXT="Cloud NAT"/>
</node>
</node>
<node CREATED="1586511154799" ID="ID_1296291608" MODIFIED="1586512084797" POSITION="right" TEXT="Cluster monitoring">
<node CREATED="1586511164273" ID="ID_1932868625" MODIFIED="1586512089140" TEXT="Monitoring the cluster">
<icon BUILTIN="closed"/>
</node>
<node CREATED="1586511170692" ID="ID_1637335468" MODIFIED="1586512091693" TEXT="Monitoring apps">
<icon BUILTIN="closed"/>
</node>
<node CREATED="1586512094904" ID="ID_633769318" MODIFIED="1586512124485" TEXT="What&apos;s deployed">
<node CREATED="1586512125305" ID="ID_61898329" MODIFIED="1586512145034" TEXT="Deployment dashboards"/>
<node CREATED="1586512146609" ID="ID_942950927" MODIFIED="1586512166262" TEXT="Ingress - Service - Pod / Deployment - ConfigMap - etc connectionss"/>
<node CREATED="1587046064346" ID="ID_1505611048" MODIFIED="1587046080925" TEXT="Generic infrastructure monitoring of Terraform resources"/>
<node CREATED="1587116055686" ID="ID_540605949" MODIFIED="1587116189159" TEXT="Check health and readiness check propagates back to GitLab">
<icon BUILTIN="idea"/>
</node>
<node CREATED="1587116097118" ID="ID_1577722665" MODIFIED="1587116194947" TEXT="GitOps">
<icon BUILTIN="idea"/>
<node CREATED="1587116125203" ID="ID_1153496803" MODIFIED="1587116126344" TEXT="move a given container version between environments"/>
<node CREATED="1587116126797" ID="ID_410054257" MODIFIED="1587116152202" TEXT="fix configuration drift"/>
<node CREATED="1587116155125" ID="ID_1146236978" MODIFIED="1587116167359" TEXT="converge on expected state"/>
</node>
</node>
</node>
<node CREATED="1586511609659" ID="ID_1998383523" MODIFIED="1586511831694" POSITION="left" TEXT="Developer focused">
<node CREATED="1586511626207" ID="ID_1281584031" MODIFIED="1586511650222" TEXT="Auto DevOps - hidden infra"/>
<node CREATED="1586511573069" ID="ID_337536134" MODIFIED="1586511580692" TEXT="Connect local dev env to pods">
<node CREATED="1586511582743" ID="ID_219391134" MODIFIED="1586511588606" TEXT="VS Code"/>
</node>
<node CREATED="1586511663823" ID="ID_407446063" MODIFIED="1586511667503" TEXT="Bespoke apps"/>
<node CREATED="1586511832747" ID="ID_1312301640" MODIFIED="1586511850629" TEXT="Managed services for persistence"/>
</node>
<node CREATED="1586511932904" ID="ID_1085383123" MODIFIED="1586511941512" POSITION="right" TEXT="Deploy GitLab to K8s">
<icon BUILTIN="closed"/>
</node>
<node CREATED="1587117272445" ID="ID_985216591" MODIFIED="1587117283035" POSITION="left" TEXT="Personas">
<node CREATED="1587117283933" ID="ID_393342055" MODIFIED="1587117288384" TEXT="Application developer">
<node CREATED="1587117387822" ID="ID_1253465221" MODIFIED="1587117393125" TEXT="Typical GitLab user"/>
</node>
<node CREATED="1587117288735" ID="ID_1915784273" MODIFIED="1587117291989" TEXT="Application operator"/>
<node CREATED="1587117292663" ID="ID_662615074" MODIFIED="1587117297349" TEXT="Platform operator">
<node CREATED="1587117298012" ID="ID_1665109588" MODIFIED="1587117306082" TEXT="self-managed GitLab"/>
<node CREATED="1587117310916" ID="ID_43309294" MODIFIED="1587117324291" TEXT="potential GitLab evangelist"/>
</node>
</node>
</node>
</map>
